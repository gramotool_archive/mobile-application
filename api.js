import axios from 'axios';

const createAPI = () => {
    const api = axios.create({
        baseURL: 'https://api.gramotool.ru',
        // baseURL: 'http://0.0.0.0:8000',
        timeout: 5000,
        headers: {
            'Authorization': 'Token 0be8ca9c0dd7e19a7ddfd18a3879d8da459ffd82',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    });

    const onSuccess = ({ data }) => data;
    const onFail = error => error;
    api.interceptors.response.use(onSuccess);

    return api;
};

export default createAPI;
