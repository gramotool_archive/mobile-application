import {PermissionsAndroid} from "react-native";


let requestExternalStoragePermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'App Storage Permission',
        message: 'App needs access to your storage ' +
          'so you can save your photos',
      },
    );
    return granted;
  } catch (err) {
    console.error('Failed to request permission ', err);
    return null;
  }
};
