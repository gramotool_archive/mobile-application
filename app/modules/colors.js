const constColors = {
    gray: '#CACDD4',
    black: '#373A42',
    purple: '#6B25FC',
    yellow: '#FEC400',
    lightgray: '#EEF3F5',
    background: '#F6F8F8',
    green: '#16C67A',
};
export default constColors;
