import {StyleSheet} from "react-native";
import constColors from "./colors";

const constStyles = StyleSheet.create({
    itemShadow: {
        shadowOffset: { width: 0, height: 4 },
        shadowColor: '#000',
        shadowOpacity: 0.07,
        shadowRadius: 15,
    },
    circleButton: {
        borderRadius: 50,
        backgroundColor: '#fff',
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    regularButton: {
        borderRadius: 8,
        backgroundColor: constColors.purple,
        paddingVertical: 20,
        width: 250,
    },
    regularButtonText: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center',
        fontFamily: "GothamPro-Bold"
    },
    h1: {
        fontSize: 28,
        marginBottom: 20,
        fontFamily: "GothamPro-Bold"
    },
    p18: {
        fontSize: 18,
    },
    p16: {
        fontSize: 16,
    },
    p14: {
        fontSize: 14,
    },
    wrapper: {
        flex: 1,
        backgroundColor: constColors.background,
    },
    container: {
        paddingHorizontal: 20,
        paddingTop:40,
    },
});

export default constStyles
