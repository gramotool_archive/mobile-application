import {ActionCreators} from "../reducers/tools/stories"
import createAPI from '../../api'

const api = createAPI();

export default function fetchStories(username) {
    return (dispatch) => {
        dispatch(ActionCreators.fetchStoriesPending());

        api.get(`/tools/account/${username}/stories/`)
        .then((response) => {
            dispatch(ActionCreators.fetchStoriesSuccess(response.stories));
        }, (error) => {
            dispatch(ActionCreators.fetchStoriesError(error.message));
        });
    }
}
