import {ActionCreators} from "../reducers/tools/avatar"
import createAPI from '../../api'

const api = createAPI();

export default function fetchHL(username) {
    return (dispatch) => {
        dispatch(ActionCreators.fetchAvatarPending());

        api.get(`/tools/account/${username}/avatar/`)
        .then((response) => {
            dispatch(ActionCreators.fetchAvatarSuccess(response.url));
        }, (error) => {
            dispatch(ActionCreators.fetchAvatarError(error.message));
        });
    }
}
