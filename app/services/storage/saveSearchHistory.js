

storage.getIdsForKey('bookmark').then(ids => {

  if (ids.includes(currentAccount.pk)) {
    storage.remove({
      key: 'bookmark',
      id: currentAccount.pk,
    });
    this.setState({inBookmark:false});
  } else {
    storage.save({
      key: 'bookmark',
      id: currentAccount.pk,
      data: currentAccount,
      expires: null
    });
    this.setState({inBookmark:true})
  }

});
