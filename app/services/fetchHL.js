import {ActionCreators} from "../reducers/tools/hl"
import createAPI from '../../api'

const api = createAPI();

function fetchHL(username) {
    return (dispatch) => {
        dispatch(ActionCreators.fetchHLPending());

        api.get(`/tools/account/${username}/highlights/`)
        .then((response) => {
            dispatch(ActionCreators.fetchHLSuccess(response.highlights));
        }, (error) => {
            dispatch(ActionCreators.fetchHLError(error.message));
        });
    }
}

function fetchHLStories(highlight_id) {
    return (dispatch) => {
        //console.log('FETCH HIGHLIGHT DETAIL '+id);
        dispatch(ActionCreators.fetchHLStoriesPending());

        api.get(`/tools/highlight/${highlight_id}/`)
        .then((response) => {
            dispatch(ActionCreators.fetchHLStoriesSuccess(response.stories));
        }, (error) => {
            dispatch(ActionCreators.fetchHLStoriesError(error.message));
        });
    }
}

export {
    fetchHL,
    fetchHLStories
}
