import {ActionCreators} from "../reducers/account/account"
import createAPI from '../../api'

const api = createAPI();

export default function fetchAccountInfo(username) {
    return (dispatch) => {
        dispatch(ActionCreators.fetchAccountInfoPending());

        api.get(`/tools/account/${username}/info/`)
            .then((response) => {
                storage.save({
                    key: 'accountInfo',
                    id: response.pk,
                    data: response,
                    expires: 1800
                });
                dispatch(ActionCreators.setCurrentAccountInfo(response));
            }, (error) => {
                dispatch(ActionCreators.fetchAccountInfoError(error.message));
            });
    }
}
