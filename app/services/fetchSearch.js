import {ActionCreators} from "../reducers/search/search"
import createAPI from '../../api'

const api = createAPI();

export default function fetchSearch(searchQuery) {
    return (dispatch) => {
        const search_query = searchQuery.toLowerCase();
        dispatch(ActionCreators.fetchSearchPending());

        api.get(`/tools/search/${search_query}/`)
        .then((response) => {
            dispatch(ActionCreators.fetchSearchSuccess(response.accounts));
        }, (error) => {
            dispatch(ActionCreators.fetchSearchError(error.message));
        });
    }
}
