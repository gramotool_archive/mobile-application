import ContentLoader, {Circle, Rect} from "react-content-loader/native";
import React from "react";

const IAccountSkeleton = () => (
    <ContentLoader
        height={80}
    >
        <Circle cx="30" cy="30" r="30" />
        <Rect x="75" y="13" rx="4" ry="4" width="100" height="13" />
        <Rect x="75" y="37" rx="4" ry="4" width="50" height="8" />
    </ContentLoader>
);

export default IAccountSkeleton;
