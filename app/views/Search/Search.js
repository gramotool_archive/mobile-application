import React, { Component } from "react";
import { connect } from 'react-redux';
import { SafeAreaView, View, Text } from "react-native";

//Skeleton
import IAccountSkeleton from "./skeletons";

//Services
import fetchSearch from "../../services/fetchSearch"

//Constants
import constStyles from "../../modules/styles";

//Components
import AccountsList from "../../components/AccountsList/AccountsList"
import SearchInput from "../../components/Search/SearchInput";
import SearchHistory from "../../components/Search/SearchHistory";
import ErrorMessage from "../../components/Errors/ErrorMessage";


class Search extends Component {

    renderSearchContent() {

        if (this.props.searchAreFetching) {
            return (
                <View style={[constStyles.container,]}>
                    <IAccountSkeleton/>
                    <IAccountSkeleton/>
                    <IAccountSkeleton/>
                </View>
            )
        }

        if (this.props.searchError) {
          var searchQuery = this.props.searchQuery;
          return (
            <ErrorMessage
              message={this.props.searchError}
              onButtonPress={() => this.props.onSearchSubmit(searchQuery)}
              buttonText={translate("refresh")}
            />
          )
        }

        if (this.props.isSearchFetchComplete) {
            if (this.props.searchResults.length > 0) {
              return (
                <View style={[{paddingBottom: 60, paddingHorizontal: 20,}]}>
                  <AccountsList
                    navigation={this.props.navigation}
                    accounts={this.props.searchResults}
                  />
                </View>
              )
            } else {
              return (
                <View style={constStyles.container}>
                  <Text style={constStyles.p18}>{translate("no_results")}</Text>
                </View>
              )
            }
        } else {
            return (
                <View style={[constStyles.container,]}>
                  <SearchHistory
                    navigation={this.props.navigation}
                  />
                </View>
            )
        }

    };

    render() {
        return (
            <SafeAreaView
              style={
                [constStyles.wrapper, this.props.searchAreFetching||this.props.searchResults.length > 0
                  ?
                  {backgroundColor:'#fff'} : {}
                ]
              }
            >
                <SearchInput/>
                {this.renderSearchContent()}
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => ({
    isSearchFetchComplete: state.SEARCH.isSearchFetchComplete,
    searchAreFetching: state.SEARCH.searchAreFetching,
    searchError: state.SEARCH.searchError,
    searchResults: state.SEARCH.searchResults,
    searchQuery: state.SEARCH.searchQuery,
});

const mapDispatchToProps = dispatch => ({
    onSearchSubmit: searchQuery => dispatch(fetchSearch(searchQuery)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);

