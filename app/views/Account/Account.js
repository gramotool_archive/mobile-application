import React, { Component } from "react";
import {View, Text, StyleSheet, Image, SafeAreaView, Animated, Dimensions} from "react-native";
import {connect} from "react-redux";

//Actions
import { ActionCreators } from "../../reducers/account/account"

//Skeleton
import AccountSkeleton from "./skeletons";

//Services
import fetchAccountInfo from "../../services/fetchAccountInfo";

//Const
import constColors from "../../modules/colors";

//Components
import Avatar from "../../components/Tools/Avatar/Avatar"
import HL from "../../components/Tools/HL/HL"
import Stories from "../../components/Tools/Stories/Stories"
import ToolButton from "../../components/Tools/ToolButton";
import ErrorMessage from "../../components/Errors/ErrorMessage";
import Topbar from "../../components/Account/Topbar";

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;


class Account extends Component {

  state = {
    scrollY: new Animated.Value(0),
    inBookmark: false,
    accountInfoHeight: 0,
  };

  onLayout = event => {
    if (this.state.dimensions) return;
    let {height} = event.nativeEvent.layout;
    this.setState({accountInfoHeight: height + 60})
  };

  componentDidMount() {
    this.getAccountInfo();

    //Check if in bookmark
    storage.getIdsForKey('bookmark').then(ids => {
      if (ids.includes(pk)) {
        this.setState({inBookmark:true});
      } else {
        this.setState({inBookmark:false})
      }
    });
  };

  getAccountInfo = () => {
    const username = this.props.currentAccount.username;
    const pk = this.props.currentAccount.pk;

    storage
      .load({
        key: 'accountInfo',
        id: pk,
      })
      .then(accountInfo => {
        this.props.setCurrentAccountInfo(accountInfo);
      })
      .catch(err => {
        this.props.fetchAccountInfo(username);
      });
  };

  toggleBookmark = () => {
    let currentAccount = this.props.currentAccount;

    storage.getIdsForKey('bookmark').then(ids => {

      if (ids.includes(currentAccount.pk)) {
        storage.remove({
          key: 'bookmark',
          id: currentAccount.pk,
        });
        this.setState({inBookmark:false});
      } else {
        storage.save({
          key: 'bookmark',
          id: currentAccount.pk,
          data: currentAccount,
          expires: null
        });
        this.setState({inBookmark:true})
      }

    });
  };

  renderContent = () => {
    if (this.props.accountInfoError) {
      return (
        <ErrorMessage
          message={this.props.accountInfoError}
          onButtonPress={this.getAccountInfo}
          buttonText={translate("refresh")}
        />
      )
    }

    if (this.props.currentAccountInfo) {
      switch(this.props.activeTool) {
        case 0:
          return (
            <Stories/>
          );

        case 1:
          return (
            <HL/>
          );

        case 2:
          return (
            <Avatar/>
          );
      }
    }

    return null;
  };

  renderBiography = () => {
    if (this.props.currentAccountInfo) {
      return (
        <Text style={styles.accountBiography}>{this.props.currentAccountInfo.biography}</Text>
      )
    } else {
      return null;
    }
  };

  renderStat = () => {
    if (this.props.currentAccountInfo) {
      return (
        <View style={styles.statContainer}>
          <View style={styles.statInfoContainer}>
            <Text style={styles.statNumber}>{ this.props.currentAccountInfo.media_count }</Text>
            <Text style={styles.statText}>{translate("posts")}</Text>
          </View>
          <View style={styles.statInfoContainer}>
            <Text style={styles.statNumber}>{ this.props.currentAccountInfo.follower_count }</Text>
            <Text style={styles.statText}>{translate("followers")}</Text>
          </View>
          <View style={styles.statInfoContainer}>
            <Text style={styles.statNumber}>{ this.props.currentAccountInfo.following_count }</Text>
            <Text style={styles.statText}>{translate("following")}</Text>
          </View>
        </View>
      )
    } else {
      return null;
    }
  };

  render() {
    if (this.props.accountInfoFetching) {
      return (
        <View>
          <SafeAreaView style={{position: 'relative'}}>
            <Topbar
              scrollY={this.state.scrollY}
              inBookmark={this.state.inBookmark}
              toggleBookmark={this.toggleBookmark}
              navigation={this.props.navigation}
              currentAccount={this.props.currentAccount}
              accountInfoHeight={this.state.accountInfoHeight}
            />
          </SafeAreaView>
          <AccountSkeleton/>
        </View>
      )
    }

    return (
      <SafeAreaView style={{position: 'relative'}}>

        <Topbar
          scrollY={this.state.scrollY}
          inBookmark={this.state.inBookmark}
          toggleBookmark={this.toggleBookmark}
          navigation={this.props.navigation}
          currentAccount={this.props.currentAccount}
          accountInfoHeight={this.state.accountInfoHeight}
        />

        <Animated.ScrollView
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
            {useNativeDriver: true}
          )}
        >
          <View
            style={styles.accountContainer}
            onLayout={this.onLayout}
          >

            <View style={styles.accountAvatarContainer}>
              <Image
                style={styles.accountAvatar}
                source={{uri: this.props.currentAccount.profile_pic_url}}
              />
            </View>

            <View style={styles.accountInfoContainer}>
              <Text style={[styles.accountFullName]}>{this.props.currentAccount.full_name}</Text>
              {this.renderBiography()}
            </View>
            {this.renderStat()}
          </View>

          <View style={styles.toolsContainer}>
            <ToolButton
              toolIndex={0}
              toolText={translate("tool_story")}
            />
            <ToolButton
              toolIndex={1}
              toolText={translate("tool_highlights")}
            />
            <ToolButton
              toolIndex={2}
              toolText={translate("tool_avatar")}
            />
          </View>
          <View style={styles.container}>
            {this.renderContent()}
          </View>

        </Animated.ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  activeTool: state.ACCOUNT.activeTool,
  accountInfoFetching: state.ACCOUNT.accountInfoFetching,
  accountInfoError: state.ACCOUNT.accountInfoError,

  currentAccount: state.ACCOUNT.currentAccount,
  currentAccountInfo: state.ACCOUNT.currentAccountInfo,
});

const mapDispatchToProps = dispatch => ({
  setCurrentAccountInfo: accountInfo => dispatch(ActionCreators.setCurrentAccountInfo(accountInfo)),
  fetchAccountInfo: username => dispatch(fetchAccountInfo(username)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Account);

const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    paddingBottom: 80,
  },
  accountContainer: {
    backgroundColor: constColors.background,
    paddingTop: 30,

    borderBottomWidth: 2,
    borderBottomColor: constColors.lightgray,
  },
  statContainer: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  statInfoContainer: {
    alignItems: 'center',
    width: displayWidth / 3 - 20,
  },
  accountAvatarContainer: {
    alignItems: 'center',
    marginBottom: 30
  },
  accountAvatar: {
    width: 80,
    height: 80,
    borderRadius: 12,

    shadowOffset: { width: 0, height: 4 },
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 15,
  },
  accountInfoContainer: {
    paddingHorizontal: 20,
    alignItems: 'center',
    marginBottom: 20,
  },
  accountFullName:{
    color: constColors.black,
    fontFamily: "GothamPro-Medium",
    fontSize: 18,
    lineHeight: 18,
    marginBottom: 10,
  },
  accountBiography: {
    color: constColors.gray,
    fontFamily: "GothamPro-Medium",
    textAlign: 'center',
    fontSize: 14,
    lineHeight: 20,
  },
  statNumber: {
    fontSize: 16,
    fontFamily: "GothamPro-Medium",
    color: constColors.black,
  },
  statText: {
    fontSize: 12,
    marginTop: 5,
    color: constColors.gray,
    fontFamily: "GothamPro-Medium",
  },
  toolsContainer: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderColor: constColors.lightgray,
  },

});
