import ContentLoader, { Rect} from "react-content-loader/native";
import React from "react";
import {Dimensions} from "react-native";

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;
const skeletonToolWidth = displayWidth/3 - 40;

const AccountSkeleton = () => (
  <ContentLoader
    height={500}
    width={displayWidth}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
  >
    <Rect x="0" y="0" rx="0" ry="0" width={displayWidth} height="200" />
    <Rect x={displayWidth - 20 - skeletonToolWidth} y="220" rx="0" ry="0" width={skeletonToolWidth} height="25" />
    <Rect x={displayWidth/2 - skeletonToolWidth/2} y="220" rx="0" ry="0" width={skeletonToolWidth} height="25" />
    <Rect x="20" y="220" rx="0" ry="0" width={skeletonToolWidth} height="25" />
  </ContentLoader>
);

export default AccountSkeleton;
