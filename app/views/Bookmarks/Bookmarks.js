import React, { Component } from "react";
import {SafeAreaView, Text, View} from "react-native";

//Components
import AccountsList from "../../components/AccountsList/AccountsList";

//Constants
import constStyles from "../../modules/styles";


class Bookmarks extends Component {
    state = {
        accounts: [],
    };

    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', (payload) => {
          storage.getAllDataForKey('bookmark').then(data => {
            this.setState({accounts: data});
          });
        });
    };

    render() {
        return (
          <SafeAreaView style={constStyles.wrapper}>
              <View style={constStyles.container}>
                  <Text style={constStyles.h1}>{translate("bookmarks")}</Text>
                  <AccountsList
                    navigation={this.props.navigation}
                    accounts={this.state.accounts}
                  />
              </View>
          </SafeAreaView>
        );
    }
}

export default Bookmarks;
