import React, {Component} from "react";
import {Text, StyleSheet} from "react-native";

//Animate
import * as Animatable from 'react-native-animatable';

//Constants
import constStyles from "../../modules/styles";

//Icons
import { Icon } from 'react-native-eva-icons';

class Alert extends Component {

  render() {
    if (this.props.isVisible) {
      return (
        <Animatable.View
          style={[styles.alertContainer, constStyles.itemShadow]}
          animation="zoomIn"
          duration={400}
        >
          <Icon name='checkmark-circle-2-outline' fill='#fff' width={25} height={25}/>
          <Text style={styles.alertText}>{translate("saved")}</Text>
        </Animatable.View>
      );
    } else {
      return null;
    }
  }
}

export default Alert;

const styles = StyleSheet.create({
  alertContainer: {
    position: 'absolute',
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: 'rgba(0,0,0,0.6)',
    borderRadius: 12,
  },
  alertText: {
    marginLeft: 15,
    fontSize: 20,
    color: '#fff',
  }
});
