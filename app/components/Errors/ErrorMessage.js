import React, { Component } from "react";
import {View, StyleSheet, Text, TouchableOpacity} from "react-native";

//Constants
import constColors from "../../modules/colors";
import constStyles from "../../modules/styles";


export default class ErrorMessage extends Component {

  get_error_message(message) {
    switch(message) {

      case ('Network Error'):
        return 'Проверьте подключение к интернету';

      default:
        return message;

    }
  };

  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.message}>{this.get_error_message(this.props.message)}</Text>
        <TouchableOpacity
          style={constStyles.regularButton}
          onPress={this.props.onButtonPress}
        >
          <Text style={constStyles.regularButtonText}>{this.props.buttonText}</Text>
        </TouchableOpacity>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    alignItems: 'center',
  },
  message: {
    color: constColors.gray,
    fontSize: 23,
    fontFamily: "GothamPro-Bold",
    marginBottom: 30,
    textAlign: 'center',
    lineHeight: 30,
  }
});
