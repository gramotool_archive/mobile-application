import React, {Component} from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Animated, SafeAreaView
} from "react-native";

//styles
import constStyles from "../../modules/styles";
import constColors from "../../modules/colors";

//Icons
import { Icon } from 'react-native-eva-icons';


class Topbar extends Component {

  renderBookmark(fillColor) {
    return (
      <TouchableOpacity onPress={this.props.toggleBookmark} >
        <Icon name={ this.props.inBookmark ? "bookmark" : "bookmark-outline"} height={25} width={25} fill={fillColor}/>
      </TouchableOpacity>
    )
  };

  render() {
    var accountInfoEnd = this.props.accountInfoHeight - 70;
    var accountInfoStart = accountInfoEnd - 50;
    var topbarBackgroundOpacity = this.props.scrollY.interpolate({
      inputRange: [accountInfoStart, accountInfoEnd],
      outputRange: [0, 1],
    });

    return (
      <SafeAreaView style={{position: 'relative'}}>

        <View style={[styles.topbar]}>
          <View style={styles.topbarContent}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" height={25} width={25} fill={constColors.black}/>
            </TouchableOpacity>
            <Text
              style={[styles.topbarUsername, constStyles.p18, {color: constColors.black}]}>{this.props.currentAccount.username}</Text>
            {this.renderBookmark(constColors.black)}
          </View>
          <Animated.View style={[styles.topbarBackground, {opacity: topbarBackgroundOpacity}]}/>
        </View>

      </SafeAreaView>
    );
  }
}

export default Topbar;

const styles = StyleSheet.create({
  topbar:{
    position: 'relative',
    height: 60,
    backgroundColor: constColors.background,
  },
  topbarContent: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 2,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  topbarBackground: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor: constColors.lightgray,
    zIndex: 1,
  },
  topbarUsername: {
    fontFamily: "GothamPro-Medium",
    color: constColors.black,
  },
});
