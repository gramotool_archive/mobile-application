import React, {Component} from "react";
import {View, Text, TouchableWithoutFeedback, Image, StyleSheet, FlatList} from "react-native";
import {connect} from "react-redux";

//Actions
import {ActionCreators as AccountActionCreators} from "../../reducers/account/account";
import {ActionCreators as StoriesActionCreators} from "../../reducers/tools/stories";
import {ActionCreators as HlActionCreators} from "../../reducers/tools/hl";
import {ActionCreators as AvatarActionCreators} from "../../reducers/tools/avatar";

//styles
import constStyles from "../../modules/styles";
import constColors from "../../modules/colors";

//Icons
import { Icon } from 'react-native-eva-icons';


class AccountsList extends Component {

    pickCurrentAccount = (accountInfo) => {
        this.props.setCurrentAccount(accountInfo);
        this.saveToHistory(accountInfo);
        this.props.clearAvatarResult();
        this.props.clearHlResult();
        this.props.clearStoriesResult();
        this.props.navigation.navigate('UserProfile');
    };

    saveToHistory(accountInfo){
        storage.getIdsForKey('searchHistory').then(ids => {
            if (ids.length > 10) {
                storage.remove({
                    key: 'searchHistory',
                    id: ids[0]
                });
            }
            storage.getIdsForKey('searchHistory').then(ids => {
                if (!ids.includes(accountInfo.pk)) {
                    storage.save({
                        key: 'searchHistory',
                        id: accountInfo.pk,
                        data: accountInfo,
                        expires: null
                    });
                }
            });
        });
    };

    render() {
        return (
            <FlatList
                data={this.props.accounts}
                keyExtractor={(item, index) => index.toString()}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}

                renderItem={({ item }) => (
                    <TouchableWithoutFeedback
                        onPress={() => this.pickCurrentAccount(item)}
                    >
                        <View style={styles.profile}>
                            <Image
                                style={styles.profilePic}
                                source={{ uri: item.profile_pic_url }}
                            />
                            <View style={styles.userInfo}>
                                <Text style={[styles.username, constStyles.p16]}>{item.username}</Text>
                                <Text style={[styles.fullName, constStyles.p14]}>{item.full_name}</Text>
                            </View>
                            <View style={styles.isPrivate}>{ item.is_private ? <Icon name='lock-outline' height={20} width={20} fill={constColors.gray}/> : null }</View>
                        </View>
                    </TouchableWithoutFeedback>
                )}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    setCurrentAccount: accountInfo => dispatch(AccountActionCreators.setCurrentAccount(accountInfo)),
    clearCurrentAccountInfo: () => dispatch(AccountActionCreators.clearCurrentAccountInfo()),

    clearStoriesResult: () => dispatch(StoriesActionCreators.clearStoriesResult()),
    clearHlResult: () => dispatch(HlActionCreators.clearHlResult()),
    clearAvatarResult: () => dispatch(AvatarActionCreators.clearAvatarResult()),
});

export default connect(null, mapDispatchToProps)(AccountsList);

const styles = StyleSheet.create({
    profile: {
        flex: 1,
        flexDirection:'row',
        marginTop: 20
    },
    profilePic: {
        borderRadius: 50,
        height: 60,
        width: 60
    },
    userInfo: {
        marginLeft: 15,
        justifyContent: 'center'
    },
    username: {
        color: constColors.black,
        fontFamily: "GothamPro-Bold"
    },
    fullName: {
        marginTop: 5,
        color: constColors.gray,
        fontFamily: "GothamPro-Medium"
    },
    isPrivate: {
        flex: 1,
        alignItems:'flex-end',
        justifyContent: 'center'
    },
});
