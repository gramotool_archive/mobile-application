import React, { Component } from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

//Components
import AccountsList from "../AccountsList/AccountsList";


class SearchHistory extends Component {
  state = {
    accounts: [],
  };

  clearSearchHistory = () => {
    storage.clearMapForKey('searchHistory');
    this.setState({'accounts':[]});
  };

  componentDidMount() {
    storage.getAllDataForKey('searchHistory').then(data => {
      this.setState({accounts:data.reverse()});
    });
  };

  render() {
    if (this.state.accounts.length > 0) {
      return (
        <View>
          <View style={styles.container}>
            <Text style={styles.h1}>{translate("recent")}</Text>
            <TouchableOpacity
              style={styles.refreshContainer}
              onPress={this.clearSearchHistory}
            >
              <Text style={styles.refreshText}>{translate("clear")}</Text>
            </TouchableOpacity>
          </View>

          <AccountsList
            navigation={this.props.navigation}
            accounts={this.state.accounts}
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

export default SearchHistory;

const styles = StyleSheet.create({
  h1: {
    fontSize: 28,
    fontFamily: "GothamPro-Bold"
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  refreshContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  refreshText: {
    color: '#BABABA',
    fontSize: 16,
  },
});
