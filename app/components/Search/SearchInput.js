import React, { Component } from "react";
import { connect } from 'react-redux';
import { View, TouchableOpacity, TextInput, StyleSheet, Dimensions, Keyboard } from "react-native";

//Actions
import {ActionCreators} from "../../reducers/search/search"

//Services
import fetchSearch from "../../services/fetchSearch"

// Icons
import {Icon} from "react-native-eva-icons";

//Constants
import constColors from "../../modules/colors";

//Animation
import * as Animatable from 'react-native-animatable';

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;

class SearchInput extends Component {

    state = {
        isClearShow: false,
        buttonWidth: 0,
        searchQuery: '',
    };

    usersSearch = () => {
        const username = this.props.searchQuery.toLowerCase();
        this.props.onSearchSubmit(username);
    };

    onChangeQuery = (text) => {
      this.props.setSearchQuery(text);

      if (text.length < 1) {
          this.setState({"isClearShow":false, "searchQuery":text});
      } else {
          this.setState({"isClearShow":true, "searchQuery":text});
      }

    };

    clearSearchInput = () => {
        this.props.clearSearchResults();
        this.setState({'searchQuery':''});
        Keyboard.dismiss();
        this.setState({"isClearShow":false});
    };

    render() {
        return (
            <View style={styles.searchHeader}>
                <View style={[styles.searchWrapper]}>
                    <Icon name='search' width={20} height={20} fill={constColors.gray} style={styles.searchIcon}/>
                    <TextInput
                        placeholder={translate("search")}
                        placeholderTextColor={constColors.gray}
                        returnKeyType='search'
                        style={[styles.searchInput]}
                        value={this.state.searchQuery}
                        onFocus={this.handleInputFocus}
                        onBlur={this.handleInputBlur}
                        onChangeText={text => this.onChangeQuery(text)}
                        onSubmitEditing={this.usersSearch}
                    />
                    <Animatable.View transition="opacity" style={{opacity: this.state.isClearShow ? 1: 0}}>
                        <TouchableOpacity style={styles.searchCloseButton} onPress={this.clearSearchInput}>
                            <Icon name='close' width={22} height={22} fill={constColors.black} />
                        </TouchableOpacity>
                    </Animatable.View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    searchQuery: state.SEARCH.searchQuery,
});

const mapDispatchToProps = dispatch => ({
    clearSearchResults: () => dispatch(ActionCreators.clearSearchResults()),
    setSearchQuery: searchQuery => dispatch(ActionCreators.setSearchQuery(searchQuery)),
    setSearchResults: searchResults => dispatch(ActionCreators.setSearchResults(searchResults)),
    onSearchSubmit: searchQuery => dispatch(fetchSearch(searchQuery)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput);

const styles = StyleSheet.create({
    searchHeader : {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: constColors.lightgray,
        backgroundColor: '#fff',
    },
    searchWrapper: {
        flexDirection:'row',
        justifyContent:'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        backgroundColor: constColors.background,
        marginHorizontal: 15,
        marginVertical: 10,
        borderRadius: 12,
        width: displayWidth - 40,
    },
    searchInput: {
        flex: 1,
        fontSize: 16,
        lineHeight: 16,
        color: constColors.black,
        fontFamily: "GothamPro",
        paddingVertical: 10,
        width: '100%',
    },
    searchIcon: {
        marginRight: 10,
    },
    searchClear: {
        marginRight: 10,
    },
    searchCloseButton: {
        height: 32,
        width: 32,
        justifyContent: 'center',
        alignItems: 'center'
    },
});
