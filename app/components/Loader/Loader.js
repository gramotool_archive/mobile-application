import React, {Component} from "react";
import {StyleSheet, ActivityIndicator, View} from "react-native";

//Constants
import constColors from "../../modules/colors";


class Loader extends Component {

  render() {
    if (this.props.isVisible) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color={constColors.purple} />
        </View>
      );
    } else {
      return null;
    }
  }
}

export default Loader;

const styles = StyleSheet.create({
  loaderContainer: {
    position: 'absolute',
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
