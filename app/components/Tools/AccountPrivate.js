import React, { Component } from "react";
import {View, StyleSheet, Text, TouchableOpacity} from "react-native";

//Components
import {Icon} from "react-native-eva-icons";

//Constants
import constColors from "../../modules/colors";
import constStyles from "../../modules/styles";


export default class AccountPrivate extends Component {

  render() {
    return(
      <View style={styles.container}>
        <Icon name="lock" height={70} width={70} fill={constColors.gray}/>
        <Text style={styles.message}>{this.props.message}</Text>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    alignItems: 'center',
  },
  message: {
    color: constColors.gray,
    fontSize: 23,
    lineHeight: 30,
    fontFamily: "GothamPro-Bold",
    marginBottom: 30,
    marginTop: 15,
    textAlign: 'center',
  }
});
