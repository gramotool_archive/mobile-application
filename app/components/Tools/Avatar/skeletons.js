import React from "react";
import ContentLoader, {Rect} from "react-content-loader/native";
import {Dimensions} from "react-native";

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;

const AvatarSkeleton = () => (
    <ContentLoader
        height={displayWidth}
        width={displayWidth}
    >
        <Rect x="20" y="0" rx="5" ry="5" width={displayWidth - 40} height={displayWidth - 40} />
    </ContentLoader>
);

export {
    AvatarSkeleton
}
