import React, { Component } from "react";
import {
    View,
    Dimensions,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Platform,
    PermissionsAndroid,
} from "react-native";
import {connect} from "react-redux";
import {Icon} from "react-native-eva-icons";
import CameraRoll from "@react-native-community/cameraroll";
import RNFetchBlob from "rn-fetch-blob";

//Components
import Alert from "../../Alert/Alert";

//Skeleton
import {AvatarSkeleton} from "./skeletons"

//Services
import fetchAvatar from "../../../services/fetchAvatar";
import constStyles from "../../../modules/styles";

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;

let requestExternalStoragePermission = async () => {
    try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
              title: 'App Storage Permission',
              message: 'App needs access to your storage ' +
                'so you can save your photos',
          },
        );
        return granted;
    } catch (err) {
        console.error('Failed to request permission ', err);
        return null;
    }
};

class Avatar extends Component {

    state = {
        isAlertVisible: false,
    };

    componentDidMount() {
        if (!this.props.isAvatarFetchComplete) {
            this.props.fetchAvatar(this.props.currentAccount.username);
        }
    }

    downloadAvatar = (uri) => {
        if (Platform.OS === 'android') {
            requestExternalStoragePermission();
        }
        RNFetchBlob
          .config({
              fileCache : true,
              appendExt : 'jpg'
          })
          .fetch('GET', uri, {})
          .then((res) => {
              CameraRoll.saveToCameraRoll(res.path());
          });

        this.showAlert();
    };

    showAlert = () => {
        this.setState({'isAlertVisible': true});
        setInterval(() => (
          this.setState({'isAlertVisible': false})
        ), 2000);
    };

    render() {
        if (this.props.avatarFetching) {
            return (
                <View style={styles.container}>
                    <AvatarSkeleton/>
                </View>
            )
        } else {
            if (this.props.avatarResult !== null) {
                return (
                  <View style={{paddingHorizontal: 20,}}>
                      <ImageBackground
                        style={{ height: (displayWidth), width: '100%' }}
                        source={{ uri: this.props.avatarResult }}
                      >
                          <View style={styles.buttonsContainer}>
                              <TouchableOpacity
                                style={[constStyles.circleButton, constStyles.itemShadow]}
                                onPress={() => this.downloadAvatar(this.props.avatarResult)}
                              >
                                  <Icon name='download' fill="#333" width={25} height={25}/>
                              </TouchableOpacity>
                          </View>
                      </ImageBackground>
                      <Alert
                        isVisible={this.state.isAlertVisible}
                      />
                  </View>
                )
            } else {
                return null;
            }

        }
    }
}

const mapStateToProps = state => ({
    currentAccount: state.ACCOUNT.currentAccount,

    avatarResult: state.AVATAR_TOOL.avatarResult,
    avatarFetching: state.AVATAR_TOOL.avatarFetching,
    avatarError: state.AVATAR_TOOL.avatarError,
    isAvatarFetchComplete: state.AVATAR_TOOL.isAvatarFetchComplete,
});

const mapDispatchToProps = dispatch => ({
    fetchAvatar: username => dispatch(fetchAvatar(username)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Avatar);

const styles = StyleSheet.create({
    buttonsContainer: {
        position: 'absolute',
        bottom: 20,
        right: 20,
    }
});
