import React from "react";
import ContentLoader, {Rect} from "react-content-loader/native";
import {Dimensions} from "react-native";

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;

const StoriesSkeleton = () => (
    <ContentLoader
        height={475}
        width={displayWidth}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    >
        <Rect x="20" y="0" rx="0" ry="0" width="120" height="25" />
        <Rect x={displayWidth - 140} y="0" rx="0" ry="0" width="120" height="25" />
        <Rect x="20" y="50" rx="0" ry="0" width={displayWidth / 2 - 30} height="300" />
        <Rect x={displayWidth - (displayWidth / 2 - 10)} y="50" rx="0" ry="0" width={displayWidth / 2 - 30} height="300" />
    </ContentLoader>
);

const StoriesWithoutBarSkeleton = () => (
    <ContentLoader
        height={475}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    >
        <Rect x="20" y="0" rx="0" ry="0" width={displayWidth / 2 - 30} height="300" />
        <Rect x={displayWidth - (displayWidth / 2 - 10)} y="0" rx="0" ry="0" width={displayWidth / 2 - 30} height="300" />
    </ContentLoader>
);

export {
    StoriesSkeleton,
    StoriesWithoutBarSkeleton
}
