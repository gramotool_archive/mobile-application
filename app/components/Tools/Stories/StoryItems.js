import React, { Component } from "react";
import { View, Dimensions, ImageBackground, StyleSheet, TouchableOpacity } from "react-native";
import constColors from "../../../modules/colors";
import {Icon} from "react-native-eva-icons";
import constStyles from "../../../modules/styles";

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;

export default class StoryItems extends Component {

    renderButtons = (story) => {
        if (story.media_type === 2) {
            return (
                <View style={styles.playButtonContainer}>
                    <View style={[constStyles.itemShadow, styles.playButton]}>
                        <Icon name='arrow-right' fill={constColors.black} width={45} height={45}/>
                    </View>
                </View>
            )
        }
    };

    render() {
        return (
            <View style={styles.storiesContainer}>
                {
                    this.props.storiesList.map(( story, key ) =>
                    (
                        <TouchableOpacity
                            key={key}
                            style={{width: displayWidth / 2 - 25, height: 300, marginBottom: 10 }}
                            onPress={() => this.props.toggleModal(story)}
                        >

                            <ImageBackground
                                source={{ uri: story.media_cover }}
                                style={styles.storyCover}
                                imageStyle={{borderRadius: 8}}
                            >
                                {this.renderButtons(story)}
                            </ImageBackground>
                        </TouchableOpacity>
                    ))
                }
            </View>
        )
    }
}


const styles = StyleSheet.create({
    storiesContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    playButtonContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.1)',
        borderRadius: 8
    },
    playButton: {
        opacity: 0.8,
        backgroundColor: '#fff',
        borderRadius: 50,
        padding: 5,
    },
    storyCover: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'cover',
    },
});
