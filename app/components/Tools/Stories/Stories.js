import React, { Component } from "react";
import {View, StyleSheet, Text} from "react-native";
import { connect } from "react-redux";

//Components
import StoryItems from "./StoryItems"
import StoryBar from "./StoryBar";
import StoryModal from "../../Modals/Story/StoryModal";
import NoContent from "../NoContent";
import AccountPrivate from "../AccountPrivate";

//Services
import fetchStories from "../../../services/fetchStories";

//Skeleton
import {StoriesSkeleton} from "./skeletons";


class Stories extends Component {

  state = {
    isModalVisible: false,
    modalContent: null,
  };

  componentDidMount() {
    if (!this.props.isStoriesFetchComplete && !this.props.currentAccount.is_private) {
      this.props.fetchStories(this.props.currentAccount.username);
    }
  }

  toggleModal = (story) => {
    this.setState({ isModalVisible: !this.state.isModalVisible, modalContent:story });
  };

  refreshStories = () => {
    this.props.fetchStories(this.props.currentAccount.username);
  };

  render() {
    if (this.props.storiesFetching) {
      return (
        <View>
          <StoriesSkeleton/>
        </View>
      )
    } else {
      if (this.props.currentAccount.is_private) {
        return (
          <AccountPrivate
            message={translate("private_story")}
          />
        )
      }

      if (this.props.storiesCount > 0) {
        return (
          <View style={{paddingHorizontal: 20,}}>
            <View>
              <StoryBar
                storiesCount={this.props.storiesResult.length}
                onRefresh={this.refreshStories}
              />
              <StoryItems
                storiesList={this.props.storiesResult}
                toggleModal={this.toggleModal}
              />
            </View>
            <StoryModal
              isModalVisible={this.state.isModalVisible}
              story={this.state.modalContent}
              toggleModal={this.toggleModal}
            />
          </View>
        )
      } else {
        return (
          <NoContent
            message={translate("has_no_story")}
            onButtonPress={this.refreshStories}
            buttonText={translate("refresh")}
          />
        )
      }
    }
  }
}

const mapStateToProps = state => ({
  currentAccount: state.ACCOUNT.currentAccount,

  storiesFetching: state.STORIES_TOOL.storiesFetching,
  storiesCount: state.STORIES_TOOL.storiesCount,
  storiesResult: state.STORIES_TOOL.storiesResult,
  storiesError: state.STORIES_TOOL.storiesError,
  isStoriesFetchComplete: state.STORIES_TOOL.isStoriesFetchComplete,
});

const mapDispatchToProps = dispatch => ({
  fetchStories: username => dispatch(fetchStories(username)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Stories);
