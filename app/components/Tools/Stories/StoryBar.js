import React, { Component } from "react";
import {View, StyleSheet, Text, TouchableOpacity} from "react-native";

//Icons
import {Icon} from "react-native-eva-icons";

//Const
import constColors from "../../../modules/colors";
import constStyles from "../../../modules/styles";


export default class StoryBar extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={constStyles.p16}>{translate("stories_count")}: {this.props.storiesCount}</Text>
                <TouchableOpacity
                    style={styles.refreshContainer}
                    onPress={this.props.onRefresh}
                >
                    <Icon name='refresh' fill={constColors.gray} width={16} height={16}/>
                    <Text style={styles.refreshText}>{translate("refresh")}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
    refreshContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    refreshText: {
        color: '#BABABA',
        marginLeft: 5,
        fontSize: 16,
    },
});
