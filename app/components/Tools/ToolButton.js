import React, { Component } from "react";
import {View, StyleSheet, Text, TouchableOpacity} from "react-native";
import { connect } from "react-redux";
import {ActionCreators} from "../../reducers/account/account";
import constColors from "../../modules/colors";


class ToolButton extends Component {

  toolClicked(index) {
    this.props.setActiveTool(index)
  };

  render() {
    return(
      <View>
        <TouchableOpacity
          onPress={() => this.toolClicked(this.props.toolIndex)}
        >
          <Text
            style={
              [styles.toolsText,
                this.props.activeTool === this.props.toolIndex ?
                  {color:constColors.black} : {color:constColors.gray}
              ]
            }
          >
            {this.props.toolText}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

}

const mapStateToProps = state => ({
  activeTool: state.ACCOUNT.activeTool,
});

const mapDispatchToProps = dispatch => ({
  setActiveTool: index => dispatch(ActionCreators.setActiveTool(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ToolButton);

const styles = StyleSheet.create({
  toolsText: {
    fontFamily: "GothamPro-Medium",
    fontSize: 14,
  },
});
