import React, { Component } from "react";
import {View, } from "react-native";
import {connect} from "react-redux";

//Components
import StoryItems from "../Stories/StoryItems";
import HLItems from "./HLItems";
import NoContent from "../NoContent";

//Skeleton
import {StoriesWithoutBarSkeleton} from "../Stories/skeletons";
import {HLLoader} from "./skeletons"

//Services
import {fetchHL, fetchHLStories} from "../../../services/fetchHL";
import StoryModal from "../../Modals/Story/StoryModal";
import AccountPrivate from "../AccountPrivate";


class HL extends Component {

    state = {
        isModalVisible: false,
        modalContent: null,
    };

    componentDidMount() {
        if (!this.props.isHlFetchComplete && !this.props.currentAccount.is_private) {
            this.props.fetchHL(this.props.currentAccount.username);
        }
    }

    toggleModal = (story) => {
        this.setState({ isModalVisible: !this.state.isModalVisible, modalContent:story });
    };

    getHLStories = (id) => {
        this.props.fetchHLStories(id)
    };

    refreshHL = () => {
        this.props.fetchHL(this.props.currentAccount.username);
    };

    renderHLStories = () => {
        if (this.props.hlStoriesFetching) {
            return (
                <StoriesWithoutBarSkeleton/>
            )
        }
        if (this.props.hlStoriesResult.length > 0) {
            return (
                <View>
                    <View style={{paddingHorizontal: 20,}}>
                        <StoryItems
                          storiesList={this.props.hlStoriesResult}
                          toggleModal={this.toggleModal}
                        />
                    </View>
                    <StoryModal
                        isModalVisible={this.state.isModalVisible}
                        story={this.state.modalContent}
                        toggleModal={this.toggleModal}
                    />
                </View>
            )
        }
    };

    render() {
        if (this.props.currentAccount.is_private) {
            return (
              <AccountPrivate
                message={translate("private_hl")}
              />
            )
        }
        if (this.props.hlFetching) {
            return (
                <HLLoader/>
            )
        }
        if (this.props.hlResult.length > 0) {
            return (
                <View>
                    <View>
                        <HLItems
                            hlList={this.props.hlResult}
                            onPress={this.getHLStories.bind(this)}
                        />
                    </View>
                    <View style={{marginTop: 40}}>
                        {this.renderHLStories()}
                    </View>
                </View>
            )
        } else {
            return (
              <NoContent
                message='У пользователя нет хайлайтов'
                onButtonPress={this.refreshHL}
                buttonText={translate("refresh")}
              />
            )
        }
    }
}

const mapStateToProps = state => ({
    currentAccount: state.ACCOUNT.currentAccount,

    hlFetching: state.HL_TOOL.hlFetching,
    hlResult: state.HL_TOOL.hlResult,
    hlError: state.HL_TOOL.hlError,
    isHlFetchComplete: state.HL_TOOL.isHlFetchComplete,

    hlStoriesFetching: state.HL_TOOL.hlStoriesFetching,
    hlStoriesResult: state.HL_TOOL.hlStoriesResult,
    hlStoriesError: state.HL_TOOL.hlStoriesError,
});

const mapDispatchToProps = dispatch => ({
    fetchHL: username => dispatch(fetchHL(username)),
    fetchHLStories: username => dispatch(fetchHLStories(username)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HL);

