import {Dimensions} from "react-native";
import ContentLoader, {Circle, Rect} from "react-content-loader/native";
import React from "react";

const dimensions = Dimensions.get('window');
const displayWidth = dimensions.width;

const HLLoader = () => (
    <ContentLoader
        height={475}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
    >
        <Circle cx={displayWidth/3 - 60} cy="40" r="40" />
        <Rect x={displayWidth/3 - 100} y="90" rx="0" ry="0" width="80" height="15" />
        <Circle cx={displayWidth/3 * 2 - 60} cy="40" r="40" />
        <Rect x={displayWidth/3 * 2 - 100} y="90" rx="4" ry="4" width="80" height="15" />
        <Circle cx={displayWidth - 60} cy="40" r="40" />
        <Rect x={displayWidth - 100} y="90" rx="4" ry="4" width="80" height="15" />
    </ContentLoader>
);

export {
    HLLoader
}
