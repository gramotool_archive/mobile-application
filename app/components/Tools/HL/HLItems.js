import React, { Component } from "react";
import {View, Image, StyleSheet, Text, TouchableOpacity, ScrollView} from "react-native";


export default class HLItems extends Component {
    render() {
        return (
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
                {
                    this.props.hlList.map((item, key) =>
                        (
                            <TouchableOpacity
                                key={key}
                                style={{position: 'relative', marginHorizontal: 15}}
                                onPress={()=>this.props.onPress(item.id)}
                            >
                                <Image
                                    source={{uri: item.cover_media.url}}
                                    style={{width: 80, height: 80, borderRadius: 50}}
                                />
                                <View style={styles.hlLabel}>
                                    <Text style={styles.storiesCount}>{item.media_count}</Text>
                                </View>
                                <Text style={styles.hlText}>{item.title}</Text>
                            </TouchableOpacity>
                        ))
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    hlLabel: {
        borderRadius:50,
        height: 35,
        width:35,
        backgroundColor: '#FEC400',
        position: 'absolute',
        top: 0,
        right: -10,
        justifyContent:'center',
        alignItems:'center'
    },
    hlText: {
        marginTop: 10,
        width: 80,
        fontFamily: "GothamPro-Medium",
        fontSize: 14,
        textAlign: 'center',
    },
    storiesCount: {
        fontSize: 14,
        fontFamily: "GothamPro-Medium",
    }
});

