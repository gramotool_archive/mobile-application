import React, { Component } from "react";
import {
  View, TouchableOpacity, StyleSheet, Platform,
  SafeAreaView, Image, Text, ImageBackground, PermissionsAndroid
} from "react-native";
import {connect} from "react-redux";
import RNFetchBlob from 'rn-fetch-blob'

import * as Animatable from 'react-native-animatable';

//Components
import CameraRoll from "@react-native-community/cameraroll";
import LinearGradient from 'react-native-linear-gradient';
import Video from 'react-native-video';
import Modal from "react-native-modal";
import {Icon} from "react-native-eva-icons";
import Alert from "../../Alert/Alert";
import Loader from "../../Loader/Loader";

//Constants
import constStyles from "../../../modules/styles";
import constColors from "../../../modules/colors";

let AnimatableTouchableOpacity = Animatable.createAnimatableComponent(TouchableOpacity);

let requestExternalStoragePermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'App Storage Permission',
        message: 'App needs access to your storage ' +
          'so you can save your photos',
      },
    );
    return granted;
  } catch (err) {
    console.error('Failed to request permission ', err);
    return null;
  }
};

class StoryModal extends Component {

    state = {
        isAlertVisible: false,
        isVideoLoading: false,
        isVideoEnd: false,
    };

    getTime = (taken_at) => {
      var now = Math.round(new Date().getTime()/1000);
      var difference = now - taken_at;

      var minutes = Math.round(difference/60);
      if (minutes < 60) {
        return minutes + translate("minute")
      }

      var hours = Math.round((difference/60)/60);
      if (hours < 24) {
        return hours + translate("hour")
      }

      var days = Math.round(((difference/60)/60)/24);
      if (days < 7) {
        return days + translate("day")
      }

      return Math.round((((difference/60)/60)/24)/7) + translate("week")
    };

    saveStory = (uri, type) => {
        if (Platform.OS === 'android') {
          requestExternalStoragePermission();
        }

        if(type === 2) {
            RNFetchBlob
              .config({
                  fileCache : true,
                  appendExt : 'mp4'
              })
              .fetch('GET', uri, {})
              .then((res) => {
                  CameraRoll.saveToCameraRoll(res.path());
              })
        } else {
          RNFetchBlob
            .config({
              fileCache : true,
              appendExt : 'jpg'
            })
            .fetch('GET', uri, {})
            .then((res) => {
              CameraRoll.saveToCameraRoll(res.path());
            });
        }
        this.showAlert();
    };

    showAlert = () => {
        this.setState({'isAlertVisible': true});
        setInterval(() => (
          this.setState({'isAlertVisible': false})
        ), 2000);
    };

    onVideoEnd(){
      this.setState({'isVideoEnd': true});
    };

    onVideoLoadStart(){
        this.setState({'isVideoLoading': true});
    };

    onVideoLoad(){
        this.setState({'isVideoLoading': false});
    };

    replayVideo = () => {
      this._video && this._video.seek(0);
      this.setState({'isVideoEnd': false});
    };

    renderContent = () => {
        if (this.props.story.media_type === 1) {
            return (
                <ImageBackground
                    style={styles.image}
                    imageStyle={styles.imageContainer}
                    source={{uri: this.props.story.media_src}}
                />
            )
        } else {
            return (
              <Video
                ref={component => (this._video = component)}
                source={{uri: this.props.story.media_src}}
                poster={this.props.story.media_cover}
                style={styles.backgroundVideo}
                resizeMode="contain"
                onLoadStart={() => this.onVideoLoadStart()}
                onLoad={() => this.onVideoLoad()}
                onEnd={() => this.onVideoEnd()}
              />
            )
        }
    };

    render() {
        if (this.props.story !== null) {
            return (
                <View>
                    <Modal
                        isVisible={this.props.isModalVisible}
                        useNativeDriver={true}
                        backdropOpacity={1}
                        animationIn={'fadeIn'}
                        onSwipeComplete={() => this.props.toggleModal(null)}
                        swipeDirection="down"
                        style={{ margin: 0 }}
                    >
                        <SafeAreaView style={{position: 'relative'}}>
                            <LinearGradient
                                style={styles.topBar}
                                colors={['rgba(0,0,0,0.6)', 'rgba(0,0,0,0)']}
                            >
                                <View style={styles.profileInfo}>
                                    <Image
                                        source={{uri: this.props.currentAccount.profile_pic_url}}
                                        style={styles.avatar}
                                    />
                                    <Text style={styles.username}>{this.props.currentAccount.username}</Text>
                                    <Text style={styles.publishTime}>{this.getTime(this.props.story.taken_at)}</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.props.toggleModal(null)}>
                                    <Icon name='close' fill="#fff" style={constStyles.itemShadow} width={30} height={30}/>
                                </TouchableOpacity>
                            </LinearGradient>
                            {this.renderContent()}

                            <View style={styles.buttonsContainer}>

                                <View style={styles.buttonsContainerLeft}>
                                  {
                                    this.state.isVideoEnd ?
                                      <AnimatableTouchableOpacity
                                        animation="fadeIn"
                                        style={[constStyles.circleButton, constStyles.itemShadow]}
                                        onPress={() => this.replayVideo()}
                                      >
                                        <Icon name='arrow-right' fill="#333" width={25} height={25}/>
                                      </AnimatableTouchableOpacity> : null
                                  }
                                </View>
                                <View style={styles.buttonsContainerRight}>
                                  <TouchableOpacity
                                    style={[constStyles.circleButton, constStyles.itemShadow]}
                                    onPress={() => this.saveStory(this.props.story.media_src, this.props.story.media_type)}
                                  >
                                    <Icon name='download' fill="#333" width={25} height={25}/>
                                  </TouchableOpacity>
                                </View>

                            </View>

                        </SafeAreaView>
                        <Loader
                            isVisible={this.state.isVideoLoading}
                        />
                        <Alert
                            isVisible={this.state.isAlertVisible}
                        />
                    </Modal>
                </View>
            )
        } else {
            return null;
        }
    }
}

const mapStateToProps = state => ({
    currentAccount: state.ACCOUNT.currentAccount,
});

export default connect(mapStateToProps, null)(StoryModal);

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: '100%'
    },
    imageContainer: {
        borderRadius: 12
    },
    backgroundVideo: {
        width: '100%',
        height: '100%',
    },
    topBar: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 1,
        padding: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    profileInfo: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    avatar: {
        borderRadius: 50,
        height: 40,
        width: 40,
        marginRight: 10,
    },
    username: {
        color: "#fff",
        marginRight: 10,
        fontFamily: "GothamPro-Medium",
    },
    publishTime: {
        color: constColors.gray,
        fontFamily: "GothamPro-Medium",
    },
    buttonsContainer: {
      position: 'absolute',
      flexDirection: 'row',
      justifyContent: 'space-between',
      bottom: 20,
      right: 0,
      left: 0,
      paddingHorizontal: 20,
    },
    buttonsContainerRight: {
      flexDirection: 'row',
    },
    buttonsContainerLeft: {
      flexDirection: 'row',
    }
});
