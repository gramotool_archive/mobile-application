const initialState = {
    activeTool: 0,

    accountInfoFetching: false,
    accountInfoError: null,
    currentAccount: null,
    currentAccountInfo: null,
};

const ActionType = {
    SET_ACTIVE_TOOL: 'SET_ACTIVE_TOOL',
    CLEAR_CURRENT_ACCOUNT_INFO: 'CLEAR_CURRENT_ACCOUNT_INFO',

    SET_CURRENT_ACCOUNT: 'SET_CURRENT_ACCOUNT',
    SET_CURRENT_ACCOUNT_INFO: 'SET_CURRENT_ACCOUNT_INFO',

    FETCH_ACCOUNT_INFO_PENDING: 'FETCH_ACCOUNT_INFO_PENDING',
    FETCH_ACCOUNT_INFO_ERROR: 'FETCH_ACCOUNT_INFO_ERROR',
};

export const ActionCreators = {
    setActiveTool: (index) => ({
        type: ActionType.SET_ACTIVE_TOOL,
        payload: index
    }),

    clearCurrentAccountInfo: () => ({
        type: ActionType.CLEAR_CURRENT_ACCOUNT_INFO
    }),

    setCurrentAccount: (accountInfo) => ({
        type: ActionType.SET_CURRENT_ACCOUNT,
        payload: accountInfo
    }),

    setCurrentAccountInfo: (accountInfo) => ({
        type: ActionType.SET_CURRENT_ACCOUNT_INFO,
        payload: accountInfo
    }),

    fetchAccountInfoPending: () => ({
        type: ActionType.FETCH_ACCOUNT_INFO_PENDING
    }),

    fetchAccountInfoError: (error) => ({
        type: ActionType.FETCH_ACCOUNT_INFO_ERROR,
        payload: error
    }),
};

export const reducer = (state = initialState, action) => {
    switch(action.type) {

        case ActionType.SET_ACTIVE_TOOL:
            return {...state, activeTool: action.payload};

        case ActionType.CLEAR_CURRENT_ACCOUNT_INFO:
            return {
                ...state,
                currentAccountInfo: null
            };

        case ActionType.SET_CURRENT_ACCOUNT:
            return {
                ...state,
                currentAccount: action.payload
            };

        case ActionType.SET_CURRENT_ACCOUNT_INFO:
            return {
                ...state,
                currentAccountInfo: action.payload,
                accountInfoFetching: false
            };

        case ActionType.FETCH_ACCOUNT_INFO_PENDING:
            return {...state, accountInfoFetching: true};

        case ActionType.FETCH_ACCOUNT_INFO_ERROR:
            return {...state, accountInfoError: action.payload, accountInfoFetching: false};

        default:
            return state;
    }
};
