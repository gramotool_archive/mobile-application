const initialState = {
    isSearchFetchComplete: false,

    searchQuery: '',
    searchAreFetching: false,
    searchError: null,
    searchResults: [],
};

const ActionType = {
    CLEAR_SEARCH_RESULT: 'CLEAR_SEARCH_RESULT',

    SET_SEARCH_QUERY: 'SET_SEARCH_QUERY',
    SET_SEARCH_RESULTS: 'SET_SEARCH_RESULTS',

    FETCH_SEARCH_PENDING: 'FETCH_SEARCH_PENDING',
    FETCH_SEARCH_SUCCESS: 'FETCH_SEARCH_SUCCESS',
    FETCH_SEARCH_ERROR: 'FETCH_SEARCH_ERROR',
};

export const ActionCreators = {
    clearSearchResults: () => ({
        type: ActionType.CLEAR_SEARCH_RESULT
    }),

    setSearchResults: searchResults => ({
        type: ActionType.SET_SEARCH_RESULTS,
        payload: searchResults
    }),

    setSearchQuery: searchQuery => ({
        type: ActionType.SET_SEARCH_QUERY,
        payload: searchQuery
    }),

    fetchSearchPending: () => ({
        type: ActionType.FETCH_SEARCH_PENDING
    }),

    fetchSearchSuccess: (searchResults) => ({
        type: ActionType.FETCH_SEARCH_SUCCESS,
        payload: searchResults
    }),

    fetchSearchError: (error) => ({
        type: ActionType.FETCH_SEARCH_ERROR,
        payload: error
    }),
};


export const reducer = (state = initialState, action) => {
    switch(action.type) {

        case ActionType.CLEAR_SEARCH_RESULT:
            return {
                ...state,
                isSearchFetchComplete: false,
                searchQuery: '',
                searchAreFetching: false,
                searchError: null,
                searchResults: []
            };

        case ActionType.SET_SEARCH_RESULTS:
            return {
                ...state,
                searchResults: action.payload
            };

        case ActionType.SET_SEARCH_QUERY:
            return {
                ...state,
                searchQuery: action.payload
            };

        case ActionType.FETCH_SEARCH_PENDING:
            return {
                ...state,
                searchAreFetching: true,
                isSearchFetchComplete: false,
                searchError: null,
            };

        case ActionType.FETCH_SEARCH_SUCCESS:
            return {
                ...state,
                searchResults: action.payload,
                searchAreFetching: false,
                isSearchFetchComplete: true,
            };

        case ActionType.FETCH_SEARCH_ERROR:
            return {
                ...state,
                searchError: action.payload,
                searchAreFetching: false,
                isSearchFetchComplete: true,
            };

        default:
            return state;
    }
};
