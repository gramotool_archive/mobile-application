const initialState = {
    isAvatarFetchComplete: false,

    avatarResult: null,
    avatarFetching: false,
    avatarError: null,
};

const ActionType = {
    FETCH_AVATAR_PENDING: 'FETCH_AVATAR_PENDING',
    FETCH_AVATAR_SUCCESS: 'FETCH_AVATAR_SUCCESS',
    FETCH_AVATAR_ERROR: 'FETCH_AVATAR_ERROR',

    CLEAR_AVATAR_RESULT: 'CLEAR_AVATAR_RESULT',
};

export const ActionCreators = {
    clearAvatarResult: () => ({
        type: ActionType.CLEAR_AVATAR_RESULT
    }),

    fetchAvatarPending: () => ({
        type: ActionType.FETCH_AVATAR_PENDING
    }),

    fetchAvatarSuccess: (avatarResult) => ({
        type: ActionType.FETCH_AVATAR_SUCCESS,
        payload: avatarResult
    }),

    fetchAvatarError: (error) => ({
        type: ActionType.FETCH_AVATAR_ERROR,
        payload: error
    }),
};

export const reducer = (state = initialState, action) => {
    switch(action.type) {

        case ActionType.CLEAR_AVATAR_RESULT:
            return {
                ...state,
                avatarResult: null,
                avatarFetching: false,
                avatarError: null,
                isAvatarFetchComplete: false,
            };

        case ActionType.FETCH_AVATAR_PENDING:
            return {
                ...state,
                avatarFetching: true,
                isAvatarFetchComplete: false,
                avatarError: null,
            };

        case ActionType.FETCH_AVATAR_SUCCESS:
            return {
                ...state,
                avatarResult: action.payload,
                avatarFetching: false,
                isAvatarFetchComplete: true,
            };

        case ActionType.FETCH_AVATAR_ERROR:
            return {
                ...state,
                avatarError: action.payload,
                avatarFetching: false,
                isAvatarFetchComplete: true,
            };

        default:
            return state;
    }
};
