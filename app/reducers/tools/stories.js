const initialState = {
    isStoriesFetchComplete: false,

    storiesFetching: false,
    storiesCount: 0,
    storiesResult: [],
    storiesError: null,
};

const ActionType = {
    FETCH_STORIES_PENDING: 'FETCH_STORIES_PENDING',
    FETCH_STORIES_SUCCESS: 'FETCH_STORIES_SUCCESS',
    FETCH_STORIES_ERROR: 'FETCH_STORIES_ERROR',

    CLEAR_STORIES_RESULT: 'CLEAR_STORIES_RESULT',
};

export const ActionCreators = {
    clearStoriesResult: () => ({
        type: ActionType.CLEAR_STORIES_RESULT
    }),

    fetchStoriesPending: () => ({
        type: ActionType.FETCH_STORIES_PENDING
    }),

    fetchStoriesSuccess: (storiesResult) => ({
        type: ActionType.FETCH_STORIES_SUCCESS,
        payload: storiesResult
    }),

    fetchStoriesError: (error) => ({
        type: ActionType.FETCH_STORIES_ERROR,
        payload: error
    }),
};

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case ActionType.CLEAR_STORIES_RESULT:
            return {
                ...state,
                storiesResult: [],
                storiesCount: 0,
                isStoriesFetchComplete: false,
                storiesFetching: false,
                storiesError: null,
            };

        case ActionType.FETCH_STORIES_PENDING:
            return {
                ...state,
                storiesFetching: true,
                isStoriesFetchComplete: false,
                storiesError: null,
            };

        case ActionType.FETCH_STORIES_SUCCESS:
            return {
                ...state,
                storiesResult: action.payload,
                storiesFetching: false,
                storiesCount: action.payload.length,
                isStoriesFetchComplete: true,
            };

        case ActionType.FETCH_STORIES_ERROR:
            return {
                ...state,
                storiesError: action.payload,
                storiesFetching: false,
                isStoriesFetchComplete: true,
            };

        default:
            return state;
    }
};
