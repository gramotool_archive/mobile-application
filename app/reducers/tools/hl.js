const initialState = {
    isHlFetchComplete: false,

    hlFetching: false,
    hlResult: [],
    hlError: null,

    hlStoriesFetching: false,
    hlStoriesResult: [],
    hlStoriesError: null,
};

const ActionType = {
    CLEAR_HL_RESULT: 'CLEAR_HL_RESULT',

    FETCH_HL_PENDING: 'FETCH_HL_PENDING',
    FETCH_HL_SUCCESS: 'FETCH_HL_SUCCESS',
    FETCH_HL_ERROR: 'FETCH_HL_ERROR',

    FETCH_HL_STORIES_PENDING: 'FETCH_HL_STORIES_PENDING',
    FETCH_HL_STORIES_SUCCESS: 'FETCH_HL_STORIES_SUCCESS',
    FETCH_HL_STORIES_ERROR: 'FETCH_HL_STORIES_ERROR',
};

export const ActionCreators = {
    clearHlResult: () => ({
        type: ActionType.CLEAR_HL_RESULT
    }),

    fetchHLPending: () => ({
        type: ActionType.FETCH_HL_PENDING
    }),

    fetchHLSuccess: (hlResult) => ({
        type: ActionType.FETCH_HL_SUCCESS,
        payload: hlResult
    }),

    fetchHLStoriesPending: () => ({
        type: ActionType.FETCH_HL_STORIES_PENDING
    }),

    fetchHLStoriesSuccess: (hlStoriesResult) => ({
        type: ActionType.FETCH_HL_STORIES_SUCCESS,
        payload: hlStoriesResult
    }),

    fetchHLStoriesError: (error) => ({
        type: ActionType.FETCH_HL_STORIES_ERROR,
        payload: error
    }),
};

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case ActionType.CLEAR_HL_RESULT:
            return {
                ...state,
                isHlFetchComplete: false,

                hlFetching: false,
                hlResult: [],
                hlError: null,

                hlStoriesFetching: false,
                hlStoriesResult: [],
                hlStoriesError: null,
            };

        case ActionType.FETCH_HL_PENDING:
            return {
                ...state,
                hlFetching: true,
                isHlFetchComplete: false,
                hlError: null,
            };

        case ActionType.FETCH_HL_SUCCESS:
            return {
                ...state,
                hlResult: action.payload,
                hlFetching: false,
                isHlFetchComplete: true,
            };

        case ActionType.FETCH_HL_ERROR:
            return {
                ...state,
                hlError: action.payload,
                hlFetching: false,
                isHlFetchComplete: true,
            };

        case ActionType.FETCH_HL_STORIES_PENDING:
            return {
                ...state,
                hlStoriesFetching: true,
                hlStoriesError: null,
            };

        case ActionType.FETCH_HL_STORIES_SUCCESS:
            return {
                ...state,
                hlStoriesResult: action.payload,
                hlStoriesFetching: false
            };

        case ActionType.FETCH_HL_STORIES_ERROR:
            return {
                ...state,
                hlStoriesError: action.payload,
                hlStoriesFetching: false
            };

        default:
            return state;
    }
};
