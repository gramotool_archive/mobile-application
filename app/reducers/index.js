import { combineReducers } from 'redux';

import { reducer as search } from './search/search';
import { reducer as account } from './account/account';
//Tools
import { reducer as avatarTool } from './tools/avatar';
import { reducer as storiesTool } from './tools/stories';
import { reducer as hlTool } from './tools/hl';

export default combineReducers({
    'SEARCH': search,
    'ACCOUNT': account,

    'AVATAR_TOOL': avatarTool,
    'STORIES_TOOL': storiesTool,
    'HL_TOOL': hlTool,
});
