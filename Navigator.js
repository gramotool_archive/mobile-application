import React from "react";

//Nav
import {createStackNavigator} from "react-navigation-stack";
import {createBottomTabNavigator} from "react-navigation-tabs";
import {createAppContainer} from "react-navigation";

//Screens
import Search from "./app/views/Search/Search";
import Account from "./app/views/Account/Account";
import Bookmarks from "./app/views/Bookmarks/Bookmarks";

//Icons
import {Icon} from "react-native-eva-icons";

//Const
import constColors from "./app/modules/colors";


const StackNavigator = createStackNavigator({
  Search: {
    screen: Search,
    navigationOptions: {
      headerShown: false,
    }
  },
  UserProfile: {
    screen: Account,
    navigationOptions: {
      headerShown: false,
    }
  },
});

const TabNavigator = createBottomTabNavigator({
  Search: {
    screen: StackNavigator,
    navigationOptions: {
      tabBarIcon: ({tintColor}) => (
        <Icon name='search' width={22} height={22} fill={tintColor}/>
      )
    }
  },
  Bookmarks: {
    screen: Bookmarks,
    navigationOptions: {
      tabBarIcon: ({tintColor}) => (
        <Icon name='bookmark-outline' width={22} height={22} fill={tintColor}/>
      )
    }
  },
}, {
  tabBarOptions: {
    showLabel: false,
    showIcon: true,
    activeTintColor: constColors.black,
    inactiveTintColor: constColors.gray,
    style: {
      backgroundColor: 'white',
      borderTopWidth: 2,
      borderTopColor: constColors.lightgray,
      paddingVertical: 20,
    }
  }
});

export default createAppContainer(TabNavigator);
